# Study plan

This project is a study plans to explore the Ruby language and its ecosystem, besides other technologies used at GitLab, such as Vue and Docker. 

## Monday

Capybara/Selenium-Ruby (to start the day)

## Tuesday

RSpec (to start the day)

## Wednesday

Rails (to start the day)

## Thursday

Design Patterns - Refactoring Guru - Ruby examples (to start the day)

## Friday

Docker (to start the day)

## Saturday

VueJS (I’m more productive at night :P)

## Sunday

GraphQL - continuation of the book The Road to GraphQL (I’m more productive at night :P)

### Tuesday and Thursday

Pure Ruby (to end the day)

- Codecademy
- https://www.ruby-lang.org/en/documentation/quickstart/
- ...
